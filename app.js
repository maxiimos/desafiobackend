let application = require('./config/server');
let cron = require("node-cron");

cron.schedule("0 */6 * * *", () => {
	application.app.controllers.matching.verifyMatch(application);
});

application.listen(3000, function(){
	console.log("Servidor rodando...");
});