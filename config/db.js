var mongo = require('mongodb');

var connectionDB = function(){
    var db = new mongo.Db(
        'avaliacao_startup',
        new mongo.Server(
            'localhost',
            27017,
            {}
        ),
        {}
    );
    return db;
}

module.exports = function(){
    return connectionDB;
}