# INFORMAÇÕES GERAIS
 - Porta: 3000
 - Endpoints: {
     http://localhost:3000/matching GET
     http://localhost:3000/tabulacoes POST
     http://localhost:3000/gravacoes POST
 }
 - Banco{
     bd: MongoDB,
     port: 27017,
     database: avaliacao_startup
     collections: {
         gravacoes,
         tabulacoes,
         matchings
     }
 }


# Exemplo body de requisições POST
- URL: http://localhost:3000/gravacoes 
- Body: {
    "telefone": "11966666666",
    "ramal": "203",
    "dataGravacao": "2020-04-12 12:34:53"
}

- URL: http://localhost:3000/tabulacoes 
- Body:   {   
    "nomeCliente": "Jõao Santos",
    "protocolo": "C202004002",
    "dataAtendimento": "2020-04-12 12:43:12",
    "numeroBinado": "11966666666",
    "numeroAcesso": "11966666666"
}
