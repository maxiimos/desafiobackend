function TabulacoesDAO(connection){
   this._connection=connection();
}

TabulacoesDAO.prototype.insert = function(tabulacao){
   return new Promise((resolve, reject) => {this._connection.open( function(err, mongoclient){
       mongoclient.collection("tabulacoes",  function(err, collection){
         collection.insert(tabulacao);
         mongoclient.close();
         if(collection){
            resolve(true);
         } else {
            resolve(false);
         }      
      }); 
   })});
}

TabulacoesDAO.prototype.list= function(){
   return new Promise((resolve, reject) => {this._connection.open( function(err, mongoclient){
      mongoclient.collection("tabulacoes",  function(err, collection){
         collection.find({}).toArray(function(err, result){
            mongoclient.close();
            if(collection){
               resolve(result);
            } else {
               resolve(err);
            }      
         });
      }); 
   })});

}


module.exports = function(){
   return TabulacoesDAO;
}