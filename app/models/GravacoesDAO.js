function GravacoesDAO(connection){
   this._connection=connection();
}

GravacoesDAO.prototype.insert = function(gravacao){
   return new Promise((resolve, reject) => {this._connection.open( function(err, mongoclient){
       mongoclient.collection("gravacoes",  function(err, collection){
         collection.insert(gravacao);
         mongoclient.close();
         if(collection){
            resolve(true);
         } else {
            resolve(false);
         }      
      }); 
   })});
}

GravacoesDAO.prototype.list = function(){
   return new Promise((resolve, reject) => {this._connection.open( function(err, mongoclient){
      mongoclient.collection("gravacoes",  function(err, collection){
         collection.find({}).toArray(function(err, result){
            mongoclient.close();
            if(collection){
               resolve(result);
            } else {
               resolve(err);
            }      
         });
      }); 
   })});

}


module.exports = function(){
   return GravacoesDAO;
}