objectId = require('mongodb').ObjectId;

function MatchingsDAO(connection){
    this._connection=connection();
 }
 
 MatchingsDAO.prototype.insert = function(matching){
    return new Promise((resolve, reject) => {this._connection.open( function(err, mongoclient){
        mongoclient.collection("matchings",  function(err, collection){
          collection.insert(matching);
          mongoclient.close();
          if(collection){
             resolve(true);
          } else {
             resolve(false);
          }      
       }); 
    })});
 }
 
 MatchingsDAO.prototype.list = function(){
    return new Promise((resolve, reject) => {this._connection.open( function(err, mongoclient){
       mongoclient.collection("matchings",  function(err, collection){
          collection.find().toArray(function(err, result){
             mongoclient.close();
             if(collection){
                resolve(result);
             } else {
                resolve(err);
             }      
          });
       }); 
    })});
 
 }
 

 MatchingsDAO.prototype.get = function(id){
    return new Promise((resolve, reject) => {this._connection.open( function(err, mongoclient){
       mongoclient.collection("matchings",  function(err, collection){
        collection.find({gravacaoId:  objectId(id)}).toArray(function(err, result){
             mongoclient.close();
             if(result.length != 0){
                resolve(false);
             } else {
                resolve(true);
             }      
          });
       }); 
    })});
 }
 
 module.exports = function(){
    return MatchingsDAO;
 }