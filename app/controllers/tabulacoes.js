var Validator = require('jsonschema').Validator;
var validator = new Validator();
var schema = {
    "id": "/Gravacao",
    "type": "object",
    "properties": {
      "nomeCliente": {"type": "string"},
      "protocolo": {"type": "string"},
      "dataAtendimento": {"type": "string"},
      "numeroBinado": {"type": "string"},
      "numeroAcesso": {"type": "string"},
    },
    "required": ["nomeCliente", "protocolo", "dataAtendimento", "numeroBinado", "numeroAcesso"]
  };
  
module.exports.insert = async function(application, req, res){

    var connection = application.config.db;
    var tabulacoesDAO = new application.app.models.TabulacoesDAO(connection);

    if(validator.validate(req.body, schema).errors != 0) {
        res.status(400).json(validator.validate(req.body, schema).errors);
    }

    let result =  await tabulacoesDAO.insert(req.body);

    if(result){
        var response = {status: "Sucesso", mensagem: "Sucesso ao incluir gravação"};
        res.status(200).json(response);
    } else {
        var response = {status: "falha", mensagem: "Falha ao incluir gravação"};
        Res.status(400).json(response);
    }
}

