var Validator = require('jsonschema').Validator;
var validator = new Validator();
var schema = {
    "id": "/Gravacao",
    "type": "object",
    "properties": {
      "telefone": {"type": "string"},
      "ramal": {"type": "string"},
      "dataGravacao": {"type": "string"}
    },
    "required": ["telefone", "ramal", "dataGravacao"]
  };

module.exports.insert = async function(application, req, res){

    var connection = application.config.db;
    var gravacoesDAO = new application.app.models.GravacoesDAO(connection);


    if(validator.validate(req.body, schema).errors != 0) {
        res.status(400).json(validator.validate(req.body, schema).errors);
    }
    let result =  await gravacoesDAO.insert(req.body);
    if(result){
        var response = {status: "Sucesso", mensagem: "Sucesso ao incluir gravação"};
        res.status(200).json(response);
    } else {
        var response = {status: "falha", mensagem: "Falha ao incluir gravação"};
        res.status(400).json(response);
    }
}

