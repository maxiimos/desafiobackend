objectId = require('mongodb').ObjectId;

module.exports.list = async function(application, req, res){
    let connection = application.config.db;
    let matchingsDAO = new application.app.models.MatchingsDAO(connection);
    let list  = await matchingsDAO.list();
    
    if(list){
        res.status(200).json(list);
    } else {
        var response = {status: "Falha", resultado: "Falha ao listar gravação"};
        res.json(response);
    }  
}

module.exports.verifyMatch = async function(application){

    let connection = application.config.db;
    let matchingsDAO = new application.app.models.MatchingsDAO(connection);
    let gravacoesDAO = new application.app.models.GravacoesDAO(connection);
    let listGravacao  = await gravacoesDAO.list();
    let tabulacoesDAO = new application.app.models.TabulacoesDAO(connection);
    let listTabulacao  = await tabulacoesDAO.list();

    let listMatchAux=[];

    for(var i=0 ; i<listGravacao.length ; i++ ){
        for(var j = 0 ; j<listTabulacao.length ; j++){
            if((listGravacao[i].telefone == listTabulacao[j].numeroBinado) && (listGravacao[i].telefone == listTabulacao[j].numeroAcesso)){
                let collect = await matchingsDAO.get(listGravacao[i]._id);
                if(collect){
                    console.log("Registros são iguais e não há registros no bd. Gravação: " + listGravacao[i].telefone + " Tabulacao: " + listTabulacao[j].numeroBinado)
                    let match = {gravacaoId: listGravacao[i]._id , tabulacaoId:  listTabulacao[j]._id}
                    listMatchAux.push(match);
                }

            }
        }
    }
    
    let response;
    
    if(listMatchAux.length > 0){
        let collect = await matchingsDAO.insert(listMatchAux);
        if(collect){
            response = {status: "Sucesso", mensagem: "Registros relacionados foram inseridos com sucesso."};
            console.log(response);
        } else {
            response = {status: "Falha", mensagem: "Falha ao inserir registros relacionados."};
            console.log(response);
        }
    } else {
        response = {status: "OK", mensagem: "Não há novos registros para inserir no banco"};
        console.log(response);
    }

}